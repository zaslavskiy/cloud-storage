const {model,Schema, ObjectId} = require('mongoose')


const File = new Schema({
  name: {type: String, required:true},
  type: {type: String, required:true},
  accessLink: {type: String},
  size:{type: Number, default:0},
  path:{type: String, default:''},
  user:{type: ObjectId, ref:'User'}, /* ссылка на юзера создавший файл*/
  parent:{type: ObjectId, ref:'File'}, /* ссылка родительскую папку*/
  children:[{type: ObjectId, ref:'File'}] /* ссылка на все вложенные файлы*/

})

module.exports = model('File', File)