import "./Navbar.scss"
import React from 'react';

import Logo from '../../assets/img/logo_Server_1.png'
import {NavLink} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {logout} from "../../reducers/userReducer";



const Navbar = () => {
  const isAuth = useSelector(store => store.user.isAuth)

  const dispatch = useDispatch();

  return (
    <div className="navbar">
      <div className="container">
        <img className={"navbar__logo"} src={Logo} alt="logo"/>
        <div className="navbar__header">TARAS CLOUD</div>
        {!isAuth && <div className="navbar__login"><NavLink to={"/login"}>Войти </NavLink></div>}
        {!isAuth && <div className="navbar__registration"><NavLink to={"/authorization"}>Регистрация </NavLink></div>}
        {isAuth && <div className="navbar__login" onClick={()=> dispatch(logout())}>Выйти </div>}
      </div>


    </div>
  );
};

export default Navbar;